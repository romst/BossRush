local levelRedirect = require "CustomOrderAPI.LevelRedirect"

levelRedirect.addLevelTransition(1, "mods/BossRush/dungeons/Lobby.xml", -1, "Lobby")

levelRedirect.setLevelTransition(1, 4, -2)
levelRedirect.setLevelTransition(2, 8, -2)
levelRedirect.setLevelTransition(3, 12, -2)
levelRedirect.setLevelTransition(4, 16, -2)
levelRedirect.setLevelTransition(5, 20, -2)

local event = require "necro.event.Event"
local levelExit = require "necro.game.tile.LevelExit"
local player = require "necro.game.character.Player"

local tile = require "necro.game.tile.Tile"
local tileTypes = require "necro.game.tile.TileTypes"

-- custom logic for lobby
event.turn.add(
    "allPlayersReadyCheck",
    {
        order = "nextTurnEffect",
        sequence = 1
    },
    function()
        local players = player.getPlayerEntities()
        local allReady = #players > 0
        for _, player in ipairs(players) do
            if player:hasComponent("position") then
                local playerTile = tile.get(player.position.x, player.position.y)
                local tileInfo = tileTypes.getTileInfo(playerTile) or {}
                if not tileInfo.wire then
                    allReady = false
                end
            end
        end

        if allReady then
            levelExit.performExit(player.getPlayerEntity(1))
        end
    end)

return levelRedirect