local entities = require "SynchronyExtendedAPI.extended.Entities"
local namedItems = require "SynchronyExtendedAPI.templates.NamedItems"

local apiUtils = require "SynchronyExtendedAPI.utils.APIUtils"

local ecs = require "system.game.Entities"

local object = require "necro.game.object.Object"
local event = require "necro.event.Event"

local seeAll = 30 * 512

entities.addItem(namedItems.Torch3, {
    name = "TorchLobby",
    data = {},
    components = {
        item = false,
        sprite = false,
        shadowPosition = false,
        visionRadial = {
            active = true,
            radius = seeAll,
        },
        lightSourceRadialHoldable = {
            innerRadiusDropped = seeAll,
            outerRadiusDropped = seeAll,
        },
        itemPerspectiveModifier = {},
    },
})

apiUtils.safeAddEvent(
        event.gameStateLevel,
        "markPlayersPVP",
        "players",
        function (ev)
            if ev.level == 1 then
                object.spawn("BossRush_TorchLobby")
            end
        end)
